//
//  PrincipalTableViewController.m
//  Incidencias
//
//  Created by Luiggi Minaya on 5/23/15.
//  Copyright (c) 2015 Luiggi Minaya. All rights reserved.
//

#import "PrincipalTableViewController.h"
#import <Parse/Parse.h>
#import "DetalleViewController.h"
#import "IncidenciaTableViewCell.h"
@interface PrincipalTableViewController ()
@property(strong,nonatomic) UIImagePickerController *picker;
@property(strong,nonatomic) UIImage*image;
@property (nonatomic, strong) NSArray * incidencias;

@end

@implementation PrincipalTableViewController
#pragma mark - Table view data source
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [super viewDidLoad];
    [self configNavigationBar];
    [self connectToParse];
    // Do any additional setup after loading the view, typically from a nib.
}
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)connectToParse {
//-------------------------------------------------------------------------------------------------------------------------------------------------
    PFQuery *query = [PFQuery queryWithClassName:@"Incidencias"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"%@", objects);
            self.incidencias=objects;
            [self.tableView reloadData];
        }
    }];

}
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)configNavigationBar {
//-------------------------------------------------------------------------------------------------------------------------------------------------
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:34.0f/255.0f green:127.0f/255.0f blue:53.0f/255.0f alpha:1.0f];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barStyle=UIBarStyleBlackOpaque;
}
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    return 1;
}
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    return [self.incidencias count];
}
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    static NSString *CellIdentifier = @"Cell";
    IncidenciaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.descriptionLabel.text=[[_incidencias objectAtIndex:indexPath.row] valueForKey:@"descripcion"];
    return cell;
}
//-------------------------------------------------------------------------------------------------------------------------------------------------
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    return @"";
}
//-------------------------------------------------------------------------------------------------------------------------------------------------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (IBAction)photoAction:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //UIImageView* imagen = [[UIImageView alloc]initWithImage:chosenImage];
    _image=chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    NSLog(@"finish");
[self performSegueWithIdentifier:@"showDetalle" sender:self];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
#pragma mark - Navigation
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    [[segue destinationViewController] setDataImageIncidencia:_image];
}
@end

