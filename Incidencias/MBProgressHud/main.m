//
//  main.m
//  MBProgressHud
//
//  Created by Luiggi Minaya on 28/12/13.
//  Copyright (c) 2013 Luiggi Minaya. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
