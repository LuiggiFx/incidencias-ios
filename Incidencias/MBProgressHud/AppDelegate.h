//
//  AppDelegate.h
//  MBProgressHud
//
//  Created by Luiggi Minaya on 28/12/13.
//  Copyright (c) 2013 Luiggi Minaya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
