//
//  ViewController.h
//  MBProgressHud
//
//  Created by Luiggi Minaya on 28/12/13.
//  Copyright (c) 2013 Luiggi Minaya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)pressHud:(id)sender;
- (IBAction)B:(id)sender;
- (IBAction)C:(id)sender;
- (IBAction)D:(id)sender;
- (IBAction)E:(id)sender;
- (IBAction)F:(id)sender;
- (IBAction)G:(id)sender;
- (IBAction)H:(id)sender;
- (IBAction)I:(id)sender;
- (IBAction)J:(id)sender;
- (IBAction)K:(id)sender;
- (IBAction)L:(id)sender;

@end
