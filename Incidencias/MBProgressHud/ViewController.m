//
//  ViewController.m
//  MBProgressHud
//
//  Created by Luiggi Minaya on 28/12/13.
//  Copyright (c) 2013 Luiggi Minaya. All rights reserved.
//

#import "ViewController.h"
#import "MBProgressHUD.h"
@interface ViewController (){
    MBProgressHUD * HUD;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark IBActions

- (IBAction)pressHud:(id)sender {
    //showSimple
    HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
}

- (IBAction)B:(id)sender {
    //showWithLabel
    HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	HUD.labelText = @"Loading";
	
	[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
}

- (IBAction)C:(id)sender {
    //showWithDetailsLabel
    HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	HUD.labelText = @"Loading";
	HUD.detailsLabelText = @"updating data";
	[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];

}

- (IBAction)D:(id)sender {
    //showWithLabelDeterminate
    HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.mode = MBProgressHUDModeDeterminate;
	HUD.labelText = @"Loading";
    [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
}

- (IBAction)E:(id)sender {
   //showWIthLabelAnnularDeterminate
    HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.labelText = @"Loading";
    [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];

	HUD.labelText = @"Loading";
}

- (IBAction)F:(id)sender {
    //Bar Determinate Mode
      HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
      HUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
      [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];

    
}

- (IBAction)G:(id)sender {
    //Custom View
    HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
	HUD.labelText = @"Completado";
	
	[HUD show:YES];
	[HUD hide:YES afterDelay:2];
}

- (IBAction)H:(id)sender {
    //Mode Swithching
     HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     HUD.labelText = @"Connecting";
     HUD.minSize = CGSizeMake(135.f, 135.f);
    [HUD showWhileExecuting:@selector(myMixedTask) onTarget:self withObject:nil animated:YES];

}

- (IBAction)I:(id)sender {
    //UsingBlocks
    #if NS_BLOCKS_AVAILABLE
    
    HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"With a block";
    
    [HUD showAnimated:YES whileExecutingBlock:^{
		[self myMixedTask];
	} completionBlock:^{
				
	}];
    
    #endif
    
}

- (IBAction)J:(id)sender {
    //showWithGradient
    HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Matriculando...";
    HUD.dimBackground = YES;
    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    
}

- (IBAction)K:(id)sender {
    //TextOnly
    HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.mode = MBProgressHUDModeText;
	HUD.labelText = @"Hola, LuiggiFx";
	HUD.margin = 10.f;
	HUD.yOffset = 0.f;  //150.f; //200.f; // 30.f;
	HUD.removeFromSuperViewOnHide = YES;
	
	[HUD hide:YES afterDelay:1.5];
}

- (IBAction)L:(id)sender {
    //With a Color
    
    HUD  = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //HUD.color = [UIColor colorWithRed:66.0f/255.0f green:79.0f/255.0f blue:91.0f/255.0f alpha:1.0f];
    HUD.color = [UIColor colorWithRed:86.0f/255.0f green:167.0f/255.0f blue:188.0f/255.0f alpha:1.0f];
    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];

}

#pragma mark Execution code

- (void)myTask {
	// Do something usefull in here instead of sleeping ...
	sleep(3);
}

- (void)myProgressTask {
	// This just increases the progress indicator in a loop
	float progress = 0.0f;
	while (progress < 1.0f) {
		progress += 0.01f;
		HUD.progress = progress;
		usleep(40000);
	}
}

- (void)myMixedTask {
	// Indeterminate mode
	sleep(2);
	// Switch to determinate mode
	HUD.mode = MBProgressHUDModeDeterminate;
	HUD.labelText = @"Progress";
	float progress = 0.0f;
	while (progress < 1.0f)
	{
		progress += 0.01f;
		HUD.progress = progress;
		usleep(50000);
	}
	// Back to indeterminate mode
	HUD.mode = MBProgressHUDModeIndeterminate;
	HUD.labelText = @"Cleaning up";
	sleep(2);
	// UIImageView is a UIKit class, we have to initialize it on the main thread
	__block UIImageView *imageView;
	dispatch_sync(dispatch_get_main_queue(), ^{
		UIImage *image = [UIImage imageNamed:@"37x-Checkmark.png"];
		imageView = [[UIImageView alloc] initWithImage:image];
	});
	HUD.mode = MBProgressHUDModeCustomView;
    HUD.customView = imageView;
	HUD.labelText = @"Completed";
	sleep(2);
}



@end
