//
//  IncidenciaTableViewCell.h
//  Incidencias
//
//  Created by Luiggi Minaya on 5/23/15.
//  Copyright (c) 2015 Luiggi Minaya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IncidenciaTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
