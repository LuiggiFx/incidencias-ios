//
//  DetalleViewController.h
//  Incidencias
//
//  Created by Luiggi Minaya on 5/23/15.
//  Copyright (c) 2015 Luiggi Minaya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetalleViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageIncidencia;
@property (strong,nonatomic ) UIImage * dataImageIncidencia;
- (IBAction)enviarAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *descriptionLabel;
@property (weak, nonatomic) IBOutlet UITextField *dniLabel;

@end
