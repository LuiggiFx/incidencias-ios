//
//  main.m
//  Incidencias
//
//  Created by Luiggi Minaya on 5/23/15.
//  Copyright (c) 2015 Luiggi Minaya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
