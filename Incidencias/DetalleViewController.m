//
//  DetalleViewController.m
//  Incidencias
//
//  Created by Luiggi Minaya on 5/23/15.
//  Copyright (c) 2015 Luiggi Minaya. All rights reserved.
//

#import "DetalleViewController.h"
#import <Parse/Parse.h>
@interface DetalleViewController ()

@end

@implementation DetalleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageIncidencia.image=self.dataImageIncidencia;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)enviarAction:(id)sender {
    
    [_dniLabel resignFirstResponder];
    [_descriptionLabel resignFirstResponder];
    
    NSString * description = [_descriptionLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString * dni = [_dniLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([dni isEqualToString:@""] || [description isEqualToString:@""] ) {
        UIAlertView * alert =[[ UIAlertView alloc] initWithTitle:nil message:@"Complete todos los datos" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        
        
    }else if ([self.dniLabel.text length]!=8) {
        UIAlertView * alert =[[ UIAlertView alloc] initWithTitle:nil message:@"El DNI debe ser de 8 dígitos" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

    }else{
        NSData *imageData = UIImagePNGRepresentation(self.dataImageIncidencia);
        PFFile *imageFile = [PFFile fileWithName:@"photo.png" data:imageData];
        
        PFObject *userPhoto = [PFObject objectWithClassName:@"Incidencias"];
        userPhoto[@"foto"] = imageFile;
        userPhoto[@"descripcion"] = description;
        userPhoto[@"dni"] = dni;

        [userPhoto saveInBackground];
        [userPhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            NSLog(@"finalizado");
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}


@end
